package main 

import(
    "net/http"
    "strings"
)

func main(){
    server := http.Server{Addr: "192.168.0.16:3000"}

    http.HandleFunc("/request-info", get_request)

    // inicializar o servidor
    server.ListenAndServe()
}

func get_request(resp http.ResponseWriter, req *http.Request){
    // req = req.ParseForm()
    host := strings.Split(req.Host, ":")
    address := host[0]
    port := host[1]
    uri := strings.Split(req.URL.String(), "?")[0]
    query_strings := strings.Split(req.URL.String(), "?")[1]
    response := "<h1>Parabéns! Você acessou a minha URL.</h1>" +
                "<h2>Seguem as informações da sua requisição:</h2>" +
                "<ul>" +
                    "<li>" + "<strong>Método: </strong><i>" + req.Method + "</i></li>" +
                    "<li>" + "<strong>Path: </strong>" +
                        "<ul>" +
                            "<li>" + "<strong>Protocolo: </strong>" + "<i>" + req.Proto + "</i>" +"</li>" +
                            "<li>" + "<strong>Endereço: </strong>" + "<i>" + address + "</i>" +"</li>" +
                            "<li>" + "<strong>Porta: </strong>" + "<i>" + port + "</i>" +"</li>" +
                            "<li>" + "<strong>URI: </strong>" + "<i>" + uri + "</i>" +"</li>"
    if len(query_strings) > 0 {
        query_strings := strings.Split(query_strings, "&")
        response +=         "<li>" + "<strong>Query Strings: </strong>" +
                                "<ul>"
                                for _, query := range query_strings {
                                    response += "<li>" + query + "</li>"
                                }
        response +=             "</ul>" +
                            "</li>" 
    }
    response +=         "</ul>" +
                    "</li>" +
                    "<li><strong>Headers: </strong>" +
                        "<ul>" 
                            for _, header := range req.Header {
                                for _, header_thing := range header {
                                    response += header_thing + "\n"
                                }
                            }
    response +=         "</ul>" +
                    "</li>" +
                "</ul"
                 

    resp.Write([]byte(response))
}
