package main

import "net/http"

func Handler01(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Boa garoto"))
}

func Handler02(w http.ResponseWriter, r *http.Request){
	w.Write([]byte("Boa garoto 2"))
}

func Handler03(w http.ResponseWriter, r *http.Request){
	if(r.Header.Get("Content-Type") != ""){
		w.Write([]byte("Incluiu o header!"))
	}else{
		w.Write([]byte("Não incluiu o header! Mas ta tranquilo!"))
	}
}