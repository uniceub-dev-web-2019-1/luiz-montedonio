package main

import ("net/http"
		"time")

func StartServer(){
	server := http.Server{Addr: "172.22.51.30:8080",
						  Handler: createRouter(),
						  ReadTimeout:  100 * time.Millisecond,
						  WriteTimeout: 200 * time.Millisecond,
						  IdleTimeout:  50 * time.Millisecond}
	server.ListenAndServe()
}