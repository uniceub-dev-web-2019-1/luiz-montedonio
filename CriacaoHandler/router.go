package main

import ( "github.com/gorilla/mux" )

func createRouter () (r *mux.Router){
	r = mux.NewRouter()
	
	r.HandleFunc("/users", Handler01).Methods("GET").Queries("name", "{anything}")

	r.HandleFunc("/users", Handler02).Methods("POST").Headers("Content-Type")

	r.HandleFunc("/users", Handler03).Methods("PUT")

	return
}